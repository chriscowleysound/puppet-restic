# == Class: restic::params
#
# Sets the default parameters for the module
#
class restic::params {
  $ensure       = 'latest'
  $package_name = 'restic'
}

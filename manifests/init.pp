# Class: restic

class restic (
  $ensure       = $::backup::params::ensure,
  $package_name = $::backup::params::package_name,
) inherits restic::params {
  class { '::restic::install': }
  ~>class { '::restic::config': }
}

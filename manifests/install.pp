# Class restic::install

class restic::install {
  if "${::osfamily}" == 'RedHat' and "${::operatingsystem}" !~ /Fedora|Amazon/ { # lint:ignore:only_variable_string
    yumrepo { 'restic':
      baseurl => "https://copr-be.cloud.fedoraproject.org/results/copart/restic/epel-${::operatingsystemmajrelease}-\$basearch/",
      descr   => 'Copr repo for restic owned by copart',
      gpgkey  => 'https://copr-be.cloud.fedoraproject.org/results/copart/restic/pubkey.gpg',
      before  => Package['restic'],
    }
  }
  package { 'restic':
    ensure => $::restic::ensure,
    name   => $::restic::package_name,
  }
}

# == Class: restic::config
#
# Configures the restic application
#
class restic::config {

  if $caller_module_name != $module_name {
    fail("Use of private class ${name} by ${caller_module_name}")
  }

  file { '/etc/restic/':
    owner => 'root',
    group => 'root',
    mode  => '0440',
  }

}

# restic


#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with restic](#setup)
    * [What restic affects](#what-restic-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with restic](#beginning-with-restic)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

Installs Restic


## Setup

### Beginning with restic

```
class { 'restic': }
```

## Usage


## Reference

## Limitations

Doesn't do much yet

## Development

Make a PR and hope for the best

